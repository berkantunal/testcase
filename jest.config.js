module.exports = {
  moduleFileExtensions: ['js', 'jsx'],
  testMatch: ['**/*.(test|spec).(js|jsx)'],
  coveragePathIgnorePatterns: ['/node_modules/', '<rootDir>/_tests_/setup.js'],
  setupFilesAfterEnv: ['<rootDir>/__tests__/setup.js'],
  coverageReporters: ['json', 'lcov', 'text', 'text-summary'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/mocks.js',
    '\\.(css|less|scss)$': '<rootDir>/__mocks__/mocks.js'
  }
}
