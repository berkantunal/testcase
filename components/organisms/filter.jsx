import React from 'react';
import PropTypes from 'prop-types';

import Autocomplete from '~/components/molecules/autocomplete';
import FormItem from '~/components/molecules/form-item';
import Button from '~/components/atoms/button';

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cityName: '',
      positionName: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(name, value) {
    this.setState(() => ({ [name]: value }));
  }

  handleClick() {
    this.props.onChange(this.state);
  }

  render() {
    return (
      <div className="sidebar__filter sticky p-3">
        <FormItem className="mb-2" label="Pozisyon">
          <Autocomplete
            column="positionName"
            onChange={value => this.handleChange('positionName', value)}
          />
        </FormItem>
        <FormItem className="mb-4" label="Şehir">
          <Autocomplete
            column="cityName"
            onChange={value => this.handleChange('cityName', value)}
          />
        </FormItem>
        <Button
          className="w-100 text-center"
          bordered
          borderColor="#810090"
          bgColor="#810090"
          onClick={this.handleClick}
        >
          ARAMA
        </Button>
      </div>
    );
  }
}

Filter.propTypes = {
  onChange: PropTypes.func
};

export default Filter;
