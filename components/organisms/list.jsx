import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';

import ListItem from '~/components/molecules/list-item';

const List = ({ list }) => {
  return list.map(item => <ListItem key={item.id} item={item} />);
};

List.propTypes = {
  list: PropTypes.array.isRequired
};

export default withRouter(List);
