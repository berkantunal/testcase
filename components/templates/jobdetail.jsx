import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import Anchor from '~/components/atoms/anchor';
import Picture from '~/components/atoms/picture';

import { getJob } from '~/client/jobs';

const Jobdetail = ({ setLoading }) => {
  const [job, setJob] = useState({});
  const router = useRouter();
  const { id } = router.query;
  setLoading(true);

  async function setData() {
    const { data: job } = await getJob(id);
    setJob(job);
  }

  useEffect(() => {
    if (id && !job.id) {
      setData();
    }
  });

  if (!job.id) {
    return null;
  }

  setLoading(false);

  return (
    <div className="row">
      <div className="col-12 mb-3">
        <div className="d-flex align-items-center">
          <Anchor to={`/joblist`}>
            <svg
              className="bi bi-chevron-left jobdetail__title--icon"
              width="1em"
              height="1em"
              viewBox="0 0 20 20"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M13.354 3.646a.5.5 0 010 .708L7.707 10l5.647 5.646a.5.5 0 01-.708.708l-6-6a.5.5 0 010-.708l6-6a.5.5 0 01.708 0z"
                clipRule="evenodd"
              ></path>
            </svg>
          </Anchor>
          <h1 className="h1 ml-3 mb-0">{job.positionName}</h1>
        </div>
        <hr />
      </div>
      <div className="col-md-3">
        <div className="jobdetail__company sticky">
          <Picture
            className="img-thumbnail"
            srcsets={[{ src: job.imageUrl, alt: job.companyName }]}
          />
          <div className="jobdetail__company-information mt-3">
            <div className="mb-1">
              <strong>Şirket</strong>: {job.companyName}
            </div>
            <div className="mb-1">
              <strong>Telefon</strong>:{job.contactPhone.countryCallingCode}
              {job.contactPhone.areaCode}
              {job.contactPhone.number}
            </div>
            <div className="">
              <strong>Adres</strong>: {job.cityName} {job.townName}
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-9">
        <div
          className="jobdetail__description"
          dangerouslySetInnerHTML={{ __html: job.description }}
        />
      </div>
    </div>
  );
};

export default Jobdetail;
