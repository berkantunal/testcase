import React from 'react';

import Filter from '~/components/organisms/filter';
import List from '~/components/organisms/list';

import { getJobs } from '~/client/jobs';

class Joblist extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      jobs: [],
      filter: {}
    };

    this.handleChangeFilter = this.handleChangeFilter.bind(this);
  }

  componentDidMount() {
    this.setJobs();
  }

  async setJobs(filter = {}) {
    try {
      this.props.setLoading(true);
      const where = {};

      if (filter.cityName) {
        where.cityName = filter.cityName;
      }

      if (filter.positionName) {
        where.positionName_like = filter.positionName;
      }

      const result = await getJobs(where);
      this.setState(() => ({ jobs: result.data }));
    } catch (error) {
      console.log('Fetch Error:', error);
    } finally {
      setTimeout(() => {
        this.props.setLoading(false);
      }, 1000);
    }
  }

  handleChangeFilter(filter) {
    this.setState(() => ({ filter }));
    this.setJobs(filter);
  }

  render() {
    const { jobs } = this.state;

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-3 mb-3 mb-md-0">
            <Filter onChange={this.handleChangeFilter} />
          </div>
          <div className="col-md-9">
            <List list={jobs} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Joblist;
