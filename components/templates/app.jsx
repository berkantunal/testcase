import React, { useState } from 'react';
import Head from 'next/head';
import Router, { withRouter } from 'next/router';

import Loader from '~/components/organisms/loader';

import '~/assets/scss/bootstrap/bootstrap.scss';
import '~/assets/scss/style.scss';

const JoblistApp = ({ Component, pageProps }) => {
  const [loading, setLoading] = useState(false);

  Router.events.on('routeChangeStart', () => {
    setLoading(true);
  });

  Router.events.on('routeChangeComplete', () => {
    setLoading(false);
  });

  return (
    <div id="main">
      <Head>
        <title>Kariyer.net</title>
      </Head>
      <Loader show={loading} />
      <div className="my-3 my-md-5">
        <div className="container">
          <Component {...pageProps} setLoading={setLoading} />
        </div>
      </div>
    </div>
  );
};

export default withRouter(JoblistApp);
