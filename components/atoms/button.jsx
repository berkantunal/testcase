import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Button = ({
  bordered,
  borderColor,
  bgColor,
  className,
  children,
  color,
  hoverColor,
  disable,
  icon,
  iconClassName,
  style,
  onClick
}) => {
  const ButtonEl = styled.button`
    background-color: ${bgColor};
    color: ${color};

    &:hover {
      color: ${hoverColor};
    }
    &.bordered {
      border-width: 2px;
      border-style: solid;
      border-color: ${borderColor};
    }
  `;

  return (
    <ButtonEl
      disable={disable}
      className={`btn ${
        bordered ? 'bordered' : null
      } ${className} d-flex align-items-center justify-content-center px-4`}
      style={style}
      onClick={onClick}
    >
      {icon ? (
        <>
          <span className={`d-flex ${iconClassName}`}>{icon}</span>
          <span className="d-flex flex-column align-items-start">{children}</span>
        </>
      ) : (
        <>{children}</>
      )}
    </ButtonEl>
  );
};

Button.defaultProps = {
  bordered: false,
  borderColor: 'currentColor',
  icon: null,
  bgColor: 'green',
  className: null,
  children: null,
  color: 'white',
  hoverColor: 'white',
  disable: false,
  iconClassName: null,
  style: {},
  onClick: () => {}
};

Button.propTypes = {
  bordered: PropTypes.bool,
  borderColor: PropTypes.string,
  icon: PropTypes.any,
  bgColor: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.any,
  color: PropTypes.string,
  hoverColor: PropTypes.string,
  disable: PropTypes.bool,
  iconClassName: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onClick: PropTypes.func
};

export default Button;
