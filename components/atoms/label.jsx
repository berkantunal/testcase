import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const LabelElement = styled.label`
  display: block;
`;

const Label = ({ children, className, style }) => {
  return (
    <LabelElement style={style} className={`${className}`}>
      {children}
    </LabelElement>
  );
};

Label.defaultProps = {
  children: null
};

Label.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any.isRequired,
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
};

export default Label;
