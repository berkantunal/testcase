import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const PictureEl = styled.picture`
  .img-fixed {
    width: 100%;
  }
`

const Picture = ({ className, srcsets }) => {
  const defaultSrcSet = srcsets[0]

  return (
    <PictureEl>
      {srcsets.length > 1
        ? srcsets.map(({ media, src }) => <source key={media} media={media} srcSet={src} />)
        : null}
      <img src={defaultSrcSet.src} alt={defaultSrcSet.alt} className={className} />
    </PictureEl>
  )
}

Picture.defaultProps = {
  className: 'img-fluid',
  srcsets: 'white'
}

Picture.propTypes = {
  className: PropTypes.string,
  srcsets: PropTypes.array.isRequired
}

export default Picture
