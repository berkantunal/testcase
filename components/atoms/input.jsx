import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ onChange, className, placeholder, value }) => {
  return (
    <input
      type="text"
      className={`input ${className}`}
      onChange={onChange}
      value={value}
      placeholder={placeholder}
    />
  );
};

Input.defaultProps = {
  className: '',
  name: '',
  placeholder: '',
  value: ''
};

Input.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string
};

export default Input;
