import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

const Anchor = ({ className, color, children, to, onClick, size, weight, style }) => (
  <Link href={to}>
    <a
      onClick={() => onClick()}
      style={style}
      className={`${className} ${color} fs-${size} ${weight}`}
    >
      {children}
    </a>
  </Link>
);

Anchor.defaultProps = {
  className: '',
  to: '/',
  color: 'white',
  onClick: () => {},
  size: 'normal',
  weight: 'regular',
  style: {}
};

Anchor.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  size: PropTypes.string,
  weight: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
};

export default Anchor;
