import React from 'react';
import PropTypes from 'prop-types';

import Picture from '~/components/atoms/picture';
import Anchor from '~/components/atoms/anchor';

const ListItem = ({ item }) => (
  <div className="card mb-3">
    <div className="row no-gutters">
      <div className="col-md-2">
        <Anchor to={`/jobdetail/${item.id}`}>
          <Picture className="card-img" srcsets={[{ src: item.imageUrl, alt: item.companyName }]} />
        </Anchor>
      </div>
      <div className="col-md-10">
        <div className="card-body">
          <h5 className="card-title">
            <Anchor to={`/jobdetail/${item.id}`}>{item.positionName}</Anchor>
          </h5>
          <p className="card-text">{item.companyName}</p>
          <p className="card-text mb-0">
            <small className="text-muted">
              {item.cityName}, {item.townName}
            </small>
          </p>
        </div>
      </div>
    </div>
  </div>
);

ListItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default ListItem;
