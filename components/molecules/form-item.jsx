import React from 'react';
import PropTypes from 'prop-types';

import Label from '~/components/atoms/label';

const FormItem = ({ children, className, label }) => {
  return (
    <div className={`sidebar__form-item ${className}`}>
      {label ? <Label>{label}</Label> : null}
      {children}
    </div>
  );
};

FormItem.propTypes = {
  label: PropTypes.string,
  className: PropTypes.string
};

export default FormItem;
