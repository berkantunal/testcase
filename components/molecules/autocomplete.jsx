import React from 'react';
import PropTypes from 'prop-types';

import { getJobs } from '~/client/jobs';

import Input from '~/components/atoms/input';

class Autocomplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      keywords: [],
      id: `autocomplete-${props.column}`
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleOptionClick = this.handleOptionClick.bind(this);
    this.windowControl = this.windowControl.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', this.windowControl);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.windowControl);
  }

  windowControl({ target }) {
    const { id, keywords } = this.state;

    if (target.closest(`#${id}`) !== null && keywords.length) {
      this.unsetOptions;
    }
  }

  handleChange({ target: { value } }) {
    this.setState(() => ({ value }));
    this.props.onChange(value);
    this.setOptions(value);
  }

  handleOptionClick(value) {
    this.setState(() => ({ value }));
    this.props.onChange(value);
    this.unsetOptions();
  }

  async setOptions(value) {
    if (!value) {
      this.unsetOptions();
      return false;
    }
    const { column } = this.props;

    const { data: jobs } = await getJobs({
      [`${column}_like`]: value,
      group_by: column
    });

    const keywords = jobs.map(job => {
      return job[column];
    });

    this.setState(() => ({ keywords }));
  }

  unsetOptions() {
    this.setState(() => ({ keywords: [] }));
  }

  render() {
    const { value, keywords, id } = this.state;

    return (
      <div className="autocomplete" id={id}>
        <Input
          className="w-100 px-2"
          name="positionName"
          value={value}
          onChange={this.handleChange}
        />
        {keywords.length > 0 && (
          <div className="autocomplete__select">
            {keywords.map(keyword => (
              <div
                key={keyword}
                onClick={() => this.handleOptionClick(keyword)}
                className="autocomplete__select--option px-2 py-1"
              >
                {keyword}
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

Autocomplete.defaultProps = {
  column: 'cityName'
};

Autocomplete.propTypes = {
  onChange: PropTypes.func,
  column: PropTypes.string
};

export default Autocomplete;
