import request from '~/utils/request';

export const getJobs = params => {
  return request.get('joblist', { params });
};

export const getJob = id => {
  return request.get(`joblist/${id}`);
};
