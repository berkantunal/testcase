import axios from 'axios';

const instance = axios.create({
  // eslint-disable-next-line
  baseURL: process.env.apiUrl,
  timeout: 5000
});

export default instance;
