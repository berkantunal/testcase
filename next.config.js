/* eslint-disable */
const path = require('path');
const withSass = require('@zeit/next-sass');
const webpack = require('webpack');

module.exports = withSass({
  env: {
    apiUrl: 'http://localhost:4000/',
  },
  compression: false,
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[local]',
  },
  webpack(config, { dev }) {
    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
        },
      },
    });

    if (dev) {
      config.module.rules.push({
        test: /\.(js|jsx)$/,
        loader: 'eslint-loader',
        exclude: ['/node_modules/', '/.next/', '/out/'],
        enforce: 'pre',
        options: {
          emitWarning: true,
        },
      });
    }

    return config;
  },
});
