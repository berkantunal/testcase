## Getting started

```bash
# clone the project
git clone https://gitlab.com/berkantunal/testcase

# enter the project directory
cd testcase

# install dependency
npm install
```

## Mock Api

```bash
# start
npm run mock:api
```

## Project

```bash
# build
npm run build

# start
npm start

Enter http://localhost:3000/joblist
```
